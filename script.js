const gameContainer = document.getElementById("game-section");
const cardContainers = document.getElementById('card-containers');


const gifList = ['gifs/1.gif', 'gifs/2.gif', 'gifs/3.gif', 'gifs/4.gif', 'gifs/5.gif', 'gifs/6.gif', 'gifs/7.gif', 'gifs/8.gif', 'gifs/9.gif', 'gifs/10.gif', 'gifs/11.gif', 'gifs/12.gif'];

// here is a helper function to shuffle an array
function shuffle(array) {
  let counter = array.length;

  while (counter > 0) {
    let index = Math.floor(Math.random() * counter);

    counter--;

    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}


// this function loops over the array of gif
function createDivsForGifs(gifList) {
  for (let gif of gifList) {

    const newDiv = document.createElement("div");
    newDiv.dataset.imgName = gif;
    newDiv.className = 'memory-card';
    const frontImage = document.createElement('img');
    frontImage.src = gif;
    frontImage.className = 'front-face';

    const backImage = document.createElement('img');
    backImage.src = '/images/question-mark.jpg';
    backImage.className = 'back-face';

    newDiv.append(frontImage, backImage);

    newDiv.addEventListener("click", handleCardClick);

    cardContainers.append(newDiv);
    gameContainer.append(cardContainers)
  }
}

createDivsForGifs(shuffle(gifList));
createDivsForGifs(shuffle(gifList));

let isCardFlipped = false;
let firstCard, secondCard;
let lockedCardContainer = false;
let scores = 0;
let flipCounts = 0;

function handleCardClick(event) {
  if (lockedCardContainer === true) {
    return;
  } else {
    if (this === firstCard) {
      return;
    } else {
      updateFlipCount(++flipCounts);
      this.classList.toggle('flip');
      if (isCardFlipped === false) {
        firstCard = this;
        isCardFlipped = true;
      } else {
        secondCard = this;
        isCardFlipped = false;
        matchingCard(firstCard, secondCard);
      }
    }
  }
}

function matchingCard(firstCard, secondCard) {
  if (firstCard.dataset.imgName === secondCard.dataset.imgName) {
    firstCard.removeEventListener('click', handleCardClick);
    secondCard.removeEventListener('click', handleCardClick);
    updateScores(++scores);
    resetCards();
  } else {
    lockedCardContainer = true;
    setTimeout(() => {
      firstCard.classList.remove('flip');
      secondCard.classList.remove('flip');
      resetCards();
    }, 1000);
  }
}

function resetCards() {
  firstCard = undefined;
  secondCard = undefined;
  isCardFlipped = false;
  lockedCardContainer = false;
}

function updateScores(scores) {
  updatingHighScoresInLocalStorage(scores);
  document.getElementById('scores').innerText = `Scores : ${scores}`;
  if (scores === 12) {
    document.getElementById('won-message').style.display = 'block';
    showingStats();
  } else {
    return false;
  }
}

function updateFlipCount(flipCounts) {
  document.getElementById('flip-counts').innerText = `Flip Counts : ${flipCounts}`;
}

function showingStats() {
  document.getElementById('card-containers').style.display = 'none';
  document.getElementsByClassName('button-control')[0].style.display = 'none';
  document.getElementsByClassName("scores-section")[0].classList.add('display-scores');
  document.getElementById('home-link').style.display = 'block';
  document.getElementById('high-scores').innerText = `High Scores : ${localStorage.getItem('highScores')}`;
}

function updatingHighScoresInLocalStorage(scores) {
  if (localStorage.getItem('highScores') === null) {
    localStorage.setItem('highScores', scores);
  } else {
    if (localStorage.getItem('highScores') < scores) {
      localStorage.setItem('highScores', scores);
    } else {
      return false;
    }
  }
}
function resetCounts() {
  scores = 0;
  flipCounts = 0;
  document.getElementById('scores').innerText = `Scores : ${scores}`;
  document.getElementById('flip-counts').innerText = `Flip Counts : ${flipCounts}`;
}

document.getElementById('exit').addEventListener('click', () => {

  document.getElementById('prompt-message').innerText = 'Are you sure you want to exit?'
  document.getElementById('prompt-div').style.display = 'block';
  

  document.getElementById('yes-btn').addEventListener('click', () => {
    document.getElementById('prompt-div').style.display = 'none';
    updatingHighScoresInLocalStorage(scores);
    showingStats();
  });

  document.getElementById('no-btn').addEventListener('click', () => {
    document.getElementById('prompt-div').style.display = 'none';
  });

});

document.getElementById('restart').addEventListener('click', () => {

  document.getElementById('prompt-message').innerText = 'Are you sure you want to restart?'
  document.getElementById('prompt-div').style.display = 'block';

  document.getElementById('yes-btn').addEventListener('click', () => {
    document.getElementById('prompt-div').style.display = 'none';
    resetCounts();
    document.getElementById('card-containers').replaceChildren();
    createDivsForGifs(shuffle(gifList));
    createDivsForGifs(shuffle(gifList));
  });

  document.getElementById('no-btn').addEventListener('click', () => {
    document.getElementById('prompt-div').style.display = 'none';
  });
});

document.getElementById('start').addEventListener('click', () => {
  document.getElementsByClassName('container')[0].style.display = 'none';
});

document.getElementById('high-scores').innerText = `High Scores : ${localStorage.getItem('highScores') ?? 0}`;
document.getElementById('won-message').style.display = 'none';
document.getElementById('home-link').style.display = 'none';
document.getElementById('prompt-div').style.display = 'none';